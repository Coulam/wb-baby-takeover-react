import React, {Component} from 'react';
import {Jumbotron, Button, Grid} from 'react-bootstrap';
import TextBlock from '../components/TextBlock/TextBlock';
import TermsConditions from '../components/TermsConditions/TermsConditions';
import Footer from '../components/Footer/Footer';
import Header from '../pages/Header/Header';
import Navigation from '../components/Navigation/Navigation';
import UploadFiles from '../components/UploadFiles/UploadFiles';
import CascadingGallery from '../components/CascadingGallery/CascadingGallery';

// Images
import Image1 from './assets/1.jpg';
import Image2 from './assets/2.jpg';

class Main extends Component {

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    shouldComponentUpdate(nextProps, nextState) {}

    componentWillUpdate(nextProps, nextState) {}

    componentDidUpdate(prevProps, prevState) {}

    componentWillUnmount() {}

    render() {

        const exampleGallery = [
            {
                name: "Joe Coulam",
                "image1": Image1,
                "image2": Image2
            }, {
                name: "John Coulam",
                "image1": "http://placehold.it/1140x500",
                "image2": "http://placehold.it/1140x500/333333/00000"
            }, {
                name: "Bob Coulam",
                "image1": "http://placehold.it/1140x500",
                "image2": "http://placehold.it/1140x500/333333/00000"
            }, {
                name: "Bob Coulam",
                "image1": "http://placehold.it/1140x500",
                "image2": "http://placehold.it/1140x500/333333/00000"
            }, {
                name: "Bob Coulam",
                "image1": "http://placehold.it/1140x500",
                "image2": "http://placehold.it/1140x500/333333/00000"
            }, {
                name: "Bob Coulam",
                "image1": "http://placehold.it/1140x500",
                "image2": "http://placehold.it/1140x500/333333/00000"
            }, {
                name: "Bob Coulam",
                "image1": "http://placehold.it/1140x500",
                "image2": "http://placehold.it/1140x500/333333/00000"
            }
        ];
        return (
            <div>

                <Header/>

                <TextBlock>
                    <h2>Intro Title Here</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
                    </p>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
                        in culpa qui officia deserunt mollit anim id est laborum</p>
                </TextBlock>

                <TextBlock bgColor="yellow">
                    <h2>Example Header</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat.
                    </p>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
                        in culpa qui officia deserunt mollit anim id est laborum</p>

                    <TermsConditions />
                        
                    <UploadFiles />
                        
                </TextBlock>

                <CascadingGallery gallery={exampleGallery} />

                <Footer/>

            </div>
        );
    }
}

export default Main;