import './Header.css';
import React from 'react';
import {Jumbotron, Grid} from 'react-bootstrap';

const Header = () => {
    return (
            <Jumbotron className="header">
                <Grid>
                    <h1>Baby Takeover</h1>
                    <p>Think your baby’s taking over your home?
                    <br />Take part in our competition.</p>
                    <img src="placehold.it/100" className="header__down-arrow" alt="Down Arrow"/>
                </Grid>
            </Jumbotron>
    );
};

export default Header;