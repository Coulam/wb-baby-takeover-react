import React, {Component} from 'react';

class InputForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.handleChange = this
            .handleChange
            .bind(this);
            
        this.handleSubmit = this
            .handleSubmit
            .bind(this);

    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
                    <input type="text" value={this.state.value} onChange={this.handleChange}/>
                </label>

                <label>
                    Select a file:
                    <input type="file" name="fileToUpload" id="fileToUpload"/>
                </label>
                <input type="submit" value="Submit"/>
            </form>
        );
    }
}

InputForm.propTypes = {};

export default InputForm;