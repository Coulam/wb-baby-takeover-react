import React, {Component} from 'react';
import {Modal, Button} from 'react-bootstrap';
import InputForm from './InputForm/InputForm';

class UploadFiles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        };
        this.close = this
            .close
            .bind(this);
        this.open = this
            .open
            .bind(this);
    }

    close() {
        this.setState({showModal: false})
    }

    open() {
        this.setState({showModal: true})
    }

    render() {
        return (
            <div>
                <Button bsStyle="primary" onClick={this.open}>
                    Upload Photos
                </Button>

                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <h2>Upload your photos</h2>
                        </Modal.Title>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore.</p>

                    </Modal.Header>
                    <Modal.Body>
                        <InputForm/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

UploadFiles.propTypes = {};

export default UploadFiles;