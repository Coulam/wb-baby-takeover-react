import React, {Component} from 'react';
import {Modal, Button} from 'react-bootstrap';
import './TermsConditions.css';

class TermsConditions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        };
        this.close = this
            .close
            .bind(this);
        this.open = this
            .open
            .bind(this);

    }

    close() {
        this.setState({showModal: false})
    }

    open() {
        this.setState({showModal: true})
    }

    render() {
        return (
            <div>
                <a onClick={this.open}>
                    <h3>Click Here to view Terms and Conditions</h3>
                </a>

                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <h2>Terms & Conditions</h2>
                        </Modal.Title>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.</p>
                    </Modal.Header>
                    <Modal.Body>
                        <h3>1. Terms & Conditions</h3>
                        <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
                            facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac,
                            vestibulum at eros.</p>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus
                            sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                        <hr/>
                        <h3>1. Terms & Conditions</h3>
                        <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
                            facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac,
                            vestibulum at eros.</p>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus
                            sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                        <hr/>
                        <h3>1. Terms & Conditions</h3>
                        <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac
                            facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac,
                            vestibulum at eros.</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

TermsConditions.propTypes = {};

export default TermsConditions;