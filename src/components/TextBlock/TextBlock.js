import React from 'react';
import {Grid, Col, Row} from 'react-bootstrap';
import './textBlock.css';
const componentName = props => {

    const bgColor = props.bgColor;
    let classColor = "white";

    if(bgColor !== undefined){
        classColor = bgColor;
    }

    return (
        <div className={`text-section bg-${classColor}`}>
            <Grid>
                <Row>
                    <Col xs={12} md={8} mdOffset={2}>
                        {props.children}
                    </Col>
                </Row>
            </Grid>
        </div>
    );
};

componentName.propTypes = {};

export default componentName;