import React from 'react';
import PropTypes from 'prop-types';
import TwentyTwenty from 'react-twentytwenty';
const GalleryImage = props => {

    return (
        <div className="gallery__container">
            <span className="gallery__name">Submitted by {props.name}</span>
            <TwentyTwenty>
                <img className="gallery__image" src={props.image1}/>
                <img className="gallery__image" src={props.image2}/>
                <div className="slider"/>
            </TwentyTwenty>
        </div>
    );
};

GalleryImage.propTypes = {};

export default GalleryImage;