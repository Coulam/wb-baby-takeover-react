import React from 'react';
import PropTypes from 'prop-types';
import GalleryImage from './GalleryImage/GalleryImage';
import {Grid, Col, Row} from 'react-bootstrap';
import './CascadingGallery.css';

const CascadingGallery = props => {

    const listItems = props
        .gallery
        .map(function (item, id) {
            return (
                <li key={id}>
                    <GalleryImage name={item.name} image1={item.image1} image2={item.image2}/>
                </li>
            );
        });

    return (
        <div className="cascading-gallery">
            <h2>User Submissions</h2>
            <Grid>
                <ul>
                    {listItems}
                </ul>
            </Grid>
        </div>
    );
};

export default CascadingGallery;