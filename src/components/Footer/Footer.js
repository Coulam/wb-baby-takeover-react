import React, {Component} from 'react';
import {
    Grid
} from 'react-bootstrap';
import './Footer.css';
import Logo from './assets/logo.svg';

class componentName extends Component {
    render() {
        return (
            <footer className="footer">
                <Grid>
                    <img src={Logo} className="footer__logo"/>
                    <p>Baby Takeover comes courtesy of Web-Blinds, a leading online store for made-to-measure blinds and shutters.</p>
                </Grid>
            </footer>
        );
    }
}

export default componentName;