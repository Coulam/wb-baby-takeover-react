import React, {Component} from 'react';

// Views //
import Main from './pages/Main';

class App extends Component {

  constructor() {
    super();
    this.state = {
      view: <Main changeView={this
          .changeView
          .bind(this)}/>
    }
  }
  changeView() {
    this.setState({
      view: <Main/>
    })
  }

  render() {
    return (
      <div className="App">
        {this.state.view}
      </div>
    );
  }
}

export default App;
